"use strict"
function filterBy(array, type){
    let filterArr = [];
    for (let arrElement of array){
        if (typeof arrElement !== type) {
            filterArr.push(arrElement);
        }
    }
    return filterArr;
}
let array = ['hello', 'world', 23, '23', null];
console.log(filterBy(array, 'string'));